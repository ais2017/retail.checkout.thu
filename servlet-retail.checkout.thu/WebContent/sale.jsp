<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>>Sale!</title>
</head>
	<body>
	
	<script>
		function checkForm(arg)
		{
			switch (arg)
			{
				case '0':	
					if (document.f.prodID.value=="" || document.f.prodQuant.value=="")
						 alert("FILL PRODUCT ID FIELD!");
			        else
					{
			        	 document.f.todo.value = "0";
			        	 document.f.submit();
					}		
					break;
				case '1':
					if (document.f.cardID.value=="" || document.f.ballsValue.value=="")
						alert("FILL CARD ID AND BALLS FIELDS!");
					else
					{
						document.f.todo.value = "1";
						document.f.submit();
					}
					break;
				case '2':
					document.f.todo.value = "2";
					document.f.submit();
					break;
				case '3':
					document.f.todo.value = "3";
					document.f.submit();
					break;
			}
		}
	</script>
	
		<form name="f" action="<%=request.getContextPath()+"/SaleServlet"%>" method="post">
		<p><b>Product ID, product quantity</b><br>
		<input type="text" size="50" name="prodID">
		<input type="text" size="50" name="prodQuant"><br>	
		<input type="button" value="Add product" onclick="checkForm('0');"><br>	
		
		<p><b>Card ID, Value of balls</b><br>
		<input type="text" size="50" name="cardID">		
		<input type="text" size="50" name="ballsValue"><br>		
		<input type="button" value="Add card" onclick="checkForm('1');"><br><br>	
			
		<input type="button" value="Pay with card" onclick="checkForm('2');">	
		<input type="button" value="Pay with cache" onclick="checkForm('3');"><br>	
		<input type="hidden" name="todo" value="zero">
		</form>		
			
	</body>
</html>
