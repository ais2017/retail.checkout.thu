<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Refund!</title>
</head>
	<script>
		function checkForm(arg)
		{
			switch (arg)
			{
				case '0':	
					if (document.f.purchID.value=="" || document.f.prodID.value=="" || document.f.prodQuant.value=="")
						 alert("FILL PRODUCT ID AND OTHERS FIELDS!");
			        else
					{
			        	 document.f.todo.value = "0";
			        	 document.f.submit();
					}		
					break;
				case '1':					
					document.f.todo.value = "1";
					document.f.submit();
					break;
			}
		}
	</script>
<body>
		<form name="f" action="<%=request.getContextPath()+"/RefundServlet"%>" method="post">
		<p><b>Purchase ID, Product ID, product quantity</b><br>
		<input type="text" size="50" name="purchID">
		<input type="text" size="50" name="prodID">
		<input type="text" size="50" name="prodQuant"><br>	
		<input type="button" value="Add product to return" onclick="checkForm('0');"><br>	
			
		<input type="button" value="Close return" onclick="checkForm('1');"><br>	
		<input type="hidden" name="todo" value="zero">
		</form>	
</body>
</html>