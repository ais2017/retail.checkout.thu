/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

/**
 *
 * @author admin
 */
public interface Stock {
    public void IncreaseQuantity(String IdProd, int incQuan) throws Exception;

    public void DecreaseQuantity(String IdProd, int decQuan) throws Exception;
}
