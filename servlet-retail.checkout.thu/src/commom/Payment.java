/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.util.*;

import java.text.*;

/**
 *
 * @author admin
 */
public class Payment {
    private  String idPay;
    private  String method;
    private  boolean status;
    private  Date date;
    private  Purchase objectOfPayment;
    
    public Payment(String IdPay, String Method, boolean status, Date date, Purchase object) throws MyException{
       
       if ((IdPay == null) || ("".equals(IdPay)))
       {
           throw new MyException("Номер операции не может быть пустым!");
       } 
        
       if ((Method == null) || ("".equals(Method)))
       {
           throw new MyException("Способ оплаты не может быть пустым!");
       } 
       
       if (object == null)
       {
           throw new MyException("Объект оплаты не может быть пустым!");
       } 
       this.idPay = IdPay;
       this.method = Method;
       this.status = status;
       this.date = date;
       this.objectOfPayment = object;
    }
    
    public Payment(String IdPay, String Method, Purchase object) throws MyException{
       
       if ((IdPay == null) || ("".equals(IdPay)))
       {
           throw new MyException("Номер операции не может быть пустым!");
       } 
        
       if ((Method == null) || ("".equals(Method)))
       {
           throw new MyException("Способ оплаты не может быть пустым!");
       } 
       
       if (object == null)
       {
           throw new MyException("Объект оплаты не может быть пустым!");
       } 
       this.idPay = IdPay;
       this.method = Method;
       this.status = false;
       this.date = new Date();
       this.objectOfPayment = object;
    }
    
    public String getIdPay() {
       return this.idPay;
    } 
      
    public String getMethod() {
       return this.method;
    } 
    
    public boolean getStatus() {
       return this.status;
    } 
    public String getDate() {
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
       String pDate = formatForDateNow.format(this.date);
       return pDate;
    } 
    
    public Purchase getObjectOfPayment() {
       return this.objectOfPayment;
    } 
    
    public void setMethod(String newMethod) throws MyException {
       
       if ((newMethod == null) || ("".equals(newMethod)))
       {
           throw new MyException("Способ оплаты не может быть пустым!");
       } 
        this.method = newMethod;
    }
    
    public void confirmPayment() throws MyException {
       
       if (this.status == true)
       {
           throw new MyException("Покупка уже оплачена!");
       } 
       this.status = true;
    }
    
    public void rejectPayment() throws MyException {
       
       if (this.status == false)
       {
           throw new MyException("Оплата уже отклонена!");
       } 
       this.status = false;
    }
    
}
