/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

/**
 *
 * @author admin
 */
public class StockImpl implements Stock {
    public Products productsImpl;
        
    public StockImpl (Products products){
        productsImpl = products;
    }
    
    @Override
    public void IncreaseQuantity(String IdProd, int incQuan) throws Exception{
        Product prodInc = productsImpl.getById(IdProd);
        if (prodInc == null) {
            throw new MyException("Товара с таким ID не существует!");
        }
        int newQuantity = prodInc.getAmount() + incQuan;
        prodInc.setAmount(newQuantity);
        productsImpl.UpdateProduct(prodInc, IdProd);
    }

    @Override
    public void DecreaseQuantity(String IdProd, int decQuan) throws Exception{
        Product prodDec = productsImpl.getById(IdProd);
        if (prodDec == null) {
            throw new MyException("Товара с таким ID не существует!");
        }
        int newQuantity = prodDec.getAmount() - decQuan;
        prodDec.setAmount(newQuantity);
        productsImpl.UpdateProduct(prodDec, IdProd);
    }
}
