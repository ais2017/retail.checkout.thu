/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Returns {
    
    public void save(Return ret) throws SQLException;

    public ArrayList<Return> ShowAllReturns() throws SQLException, MyException;

    public Return getById(Long id) throws SQLException, MyException;
    
    void UpdateReturn (Return newReturn, Long Id) throws Exception;
}
