package commom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import com.microsoft.sqlserver.jdbc.*;

public class PurchaseSQL implements Purchases {

	Connection con = null;
	String conUrl = "jdbc:sqlserver://127.0.0.1; databaseName=retail_checkout_thu; user=SA; password=1Qwerty7;";
	Statement statement = null;
	Statement statement2 = null;
	Statement statement3 = null;
	
	public PurchaseSQL() throws SQLException, ClassNotFoundException {
		DriverManager.registerDriver(new SQLServerDriver());
//		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver.class"); 
		con = DriverManager.getConnection(conUrl);
		statement = con.createStatement();
		statement2 = con.createStatement();
		statement3 = con.createStatement();
	}

	@Override
	public void save(Purchase purchase) throws SQLException, MyException {
		int status = 0;
		String idCard = "";
		
		if (purchase.getStatus()) status = 1;
		if (purchase.getCard() != null) idCard = purchase.getCard().getId();
//		if (purchase.getPayment() != null) idPay = purchase.getPayment().getIdPay();
		statement.executeUpdate("INSERT INTO Purchase VALUES ("  + purchase.getID() + ", '" + idCard + "', " + purchase.getCutPoints() + ", " + status + ");");
		for (Entry<String, SelectedProduct> entry : purchase.getList().entrySet())
			statement.executeUpdate("INSERT INTO SelectedProduct VALUES ('" + entry.getValue().getProduct().getId() + "', " + purchase.getID() + ", " + entry.getValue().getQuantity() + ");");
	}

	@Override
	public ArrayList<Purchase> ShowAllPurchases() throws SQLException, MyException {
		ArrayList<Purchase> arrayList = new ArrayList<Purchase>();
		Map <String, SelectedProduct> sprods;
		ResultSet resSetPurchases = statement.executeQuery("SELECT * FROM Purchase");
		ResultSet resSetSavedProducts;
		ResultSet resSetProducts;
		
		while (resSetPurchases.next() != false) {
			sprods = new HashMap<String, SelectedProduct>();
			resSetSavedProducts = statement2.executeQuery("SELECT * FROM SelectedProduct WHERE purchaseID = " + resSetPurchases.getInt(1));
			while (resSetSavedProducts.next() != false) {
				resSetProducts = statement3.executeQuery("SELECT * FROM Product WHERE id LIKE '" + resSetSavedProducts.getString(1) + "';");
				if (resSetProducts.next() != false)
					sprods.put(resSetSavedProducts.getString(1), new SelectedProduct(new Product(resSetProducts.getString(1), resSetProducts.getBigDecimal(2), resSetProducts.getInt(3)), resSetSavedProducts.getInt(3)));
			}
			arrayList.add(new Purchase(resSetPurchases.getInt(1), sprods));
		}
		return arrayList;
	}

	@Override
	public Purchase getById(Long id) throws SQLException, MyException {
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Purchase WHERE purchaseID = " + id);
		ResultSet resSetSavedProducts;
		ResultSet resSetProducts;
		ResultSet resSetCard;
		Card card = null;
		
		Map <String, SelectedProduct> sprods = new HashMap<String, SelectedProduct>();
		
		if (resultSet.next() != false) {
			sprods = new HashMap<String, SelectedProduct>();
			resSetSavedProducts = statement2.executeQuery("SELECT * FROM SelectedProduct WHERE purchaseID = " + resultSet.getInt(1));
			while (resSetSavedProducts.next() != false) {
				resSetProducts = statement3.executeQuery("SELECT * FROM Product WHERE id LIKE '" + resSetSavedProducts.getString(1) + "'");
				if (resSetProducts.next() != false) {
					sprods.put(resSetSavedProducts.getString(1), new SelectedProduct(new Product(resSetProducts.getString(1), resSetProducts.getBigDecimal(2), resSetProducts.getInt(3)), resSetSavedProducts.getInt(3)));
				}
			}
			if (resultSet.getString(2) != null) {
				resSetCard = statement2.executeQuery("SELECT * FROM Card WHERE id_card LIKE '" + resultSet.getString(2) + "';");
				if (resSetCard.next() != false)
					card = new Card(resSetCard.getString(1), resSetCard.getInt(2));
			}
			return new Purchase(id.intValue(), sprods, card, resultSet.getInt(3), resultSet.getBoolean(4));
		}
		return null;
	}

	@Override
	public void UpdatePurchase(Purchase newPurch, Long Id) throws Exception {
		short status = 0;
		
		if (newPurch.getStatus()) status = 1;
		if (newPurch.getCard() != null) {
		statement.executeUpdate("UPDATE Purchase SET id_card = '" + newPurch.getCard().getId() + 
												  "', cutPoints = " + newPurch.getCutPoints() + 
												  ", status = " + status + " WHERE purchaseID = "+ newPurch.getID() + ";");
		} else if (newPurch.getCard() == null) {
			statement.executeUpdate("UPDATE Purchase SET cutPoints = " + newPurch.getCutPoints() + 
					  ", status = " + status + " WHERE purchaseID = "+ newPurch.getID() + ";");
		}
		for (Entry<String, SelectedProduct> entry : newPurch.getList().entrySet())
			try {
				statement.executeUpdate("INSERT INTO SelectedProduct VALUES ('" + entry.getValue().getProduct().getId() + "', " + newPurch.getID() + ", " + entry.getValue().getQuantity() + ");");
			} catch (SQLException e) {
				// TODO: handle exception
			}			
	}
}
