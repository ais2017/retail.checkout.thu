/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Cards {
    public void CreateCardList () throws Exception;

    public ArrayList<Card> ShowAllCards() throws SQLException, MyException;

    public Card getById(String id) throws SQLException, MyException;
    
    void UpdateCard (Card newCard) throws Exception;
}
