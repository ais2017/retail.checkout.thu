package commom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ProductsSQL implements Products {

	Connection con = null;
	String conUrl = "jdbc:sqlserver://127.0.0.1; databaseName=retail_checkout_thu; user=SA; password=1Qwerty7;";
	Statement statement = null;
	
	public ProductsSQL() throws SQLException {
		con = DriverManager.getConnection(conUrl);
		statement = con.createStatement();
	}

	@Override
	public void CreateProductList() throws Exception {}

	@Override
	public ArrayList<Product> ShowAllProducts() throws MyException, SQLException {
//		ArrayList<Product> arrayList = new ArrayList<Product>();
//		ResultSet resultSet = statement.executeQuery("SELECT * FROM Product");
//		
//		while (resultSet.next() != false) {
//			arrayList.add(new Product(resultSet.getString(1), resultSet.getBigDecimal(2), resultSet.getInt(3)));
//		}
//		return arrayList;
		return null;
	}

	@Override
	public Product getById(String id) throws SQLException, MyException {
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Product WHERE id LIKE '" + id + "'");
		
		if (resultSet.next() != false)
			return new Product(resultSet.getString(1), resultSet.getBigDecimal(2), resultSet.getInt(3));
		return null;
	}

	@Override
	public void UpdateProduct(Product newProduct, String Id) throws Exception {
		statement.executeUpdate("UPDATE Product SET price = " + newProduct.getPrice() + ", amount = " + newProduct.getAmount() + " WHERE id LIKE '" + newProduct.getId() + "'");
	}
}
