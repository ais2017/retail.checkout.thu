package commom;

import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.*;

public class CardsSQL implements Cards {

	Connection con = null;
	String conUrl = "jdbc:sqlserver://127.0.0.1; databaseName=retail_checkout_thu; user=SA; password=1Qwerty7;";
	Statement statement = null;
	
	public CardsSQL() throws SQLException {
		con = DriverManager.getConnection(conUrl);
		statement = con.createStatement();
	}
	
	@Override
	public void CreateCardList() throws Exception {}

	@Override
	public ArrayList<Card> ShowAllCards() throws SQLException, MyException {
//		ArrayList<Card> arrayList = new ArrayList<Card>();
//		ResultSet resultSet = statement.executeQuery("SELECT * FROM Card");
//		
//		while (resultSet.next() != false) {
//			arrayList.add(new Card(resultSet.getString(1), resultSet.getInt(2)));
//		}
//		return arrayList;
		return null;
	}

	@Override
	public Card getById(String id) throws SQLException, MyException {
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Card WHERE id_card = '" + id + "'");
		
		if (resultSet.next() != false)
			return new Card(resultSet.getString(1), resultSet.getInt(2));
		return null;
	}

	@Override
	public void UpdateCard(Card newCard) throws Exception {
		statement.executeUpdate("UPDATE Card SET quantity = " + newCard.getQuantity() + " WHERE id_card = '" + newCard.getId() + "'");
	}

}
