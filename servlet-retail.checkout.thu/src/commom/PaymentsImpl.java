/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author admin
 */
public class PaymentsImpl implements Payments {
    public Map<String, Payment> payments;
    
    public PaymentsImpl (){
        this.payments=new HashMap<>();
    }
    
    @Override
    public void save(Payment pay) {
        payments.put(pay.getIdPay(), pay);
    }

    @Override
    public Payment getById(String id) {
        return payments.get(id);
    }

    @Override
    public ArrayList<Payment> ShowAllPayments () {
        ArrayList ListOfPayments = new ArrayList<>(payments.entrySet());
        return ListOfPayments;
    }
    
    @Override
    public void UpdatePayment(Payment newPay) throws Exception {
        if (getById(newPay.getIdPay())!=null) {
            payments.put(newPay.getIdPay(), newPay);
        }
        else
            throw new Exception();
    }
}
