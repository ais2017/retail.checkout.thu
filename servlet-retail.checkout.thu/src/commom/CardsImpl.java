/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.util.*;

/**
 *
 * @author admin
 */
public class CardsImpl implements Cards {
    public Map<String, Card> cards;
    
    public CardsImpl (){
        this.cards=new HashMap<>();
    }

    @Override
    public void CreateCardList() throws Exception{
        Card card1 = new Card("1234 5678 7777 1112", 500);
        Card card2 = new Card("1234 5678 7632 1112", 300);
        Card card3 = new Card("1234 2656 7777 1112", 700);
        Card card4 = new Card("2985 5678 7777 1112", 100);
        Card card5 = new Card("1234 5678 7777 2378", 50);
        cards.put(card1.getId(), card1);
        cards.put(card2.getId(), card2);
        cards.put(card3.getId(), card3);
        cards.put(card4.getId(), card4);
        cards.put(card5.getId(), card5);
    }

    @Override
    public Card getById(String id) {
        return cards.get(id);
    }

    @Override
    public ArrayList<Card> ShowAllCards () {
        ArrayList ListOfCards = new ArrayList<>(cards.entrySet());
        return ListOfCards;
    }
    
    @Override
     public void UpdateCard (Card newCard) throws Exception {
        if (getById(newCard.getId())!=null) {
            cards.put(newCard.getId(), newCard);
        }
        else
            throw new Exception();
    }

}
