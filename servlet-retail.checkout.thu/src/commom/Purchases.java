/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Purchases {
    public void save(commom.Purchase newPurchase) throws SQLException, MyException;

    public ArrayList<Purchase> ShowAllPurchases() throws SQLException, MyException;

    public Purchase getById(Long id) throws SQLException, MyException;
    
    void UpdatePurchase (Purchase newPurch, Long Id) throws Exception;
    
}
