/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Payments {
    public void save(Payment pay) throws SQLException;

    public ArrayList<Payment> ShowAllPayments() throws SQLException;

    public Payment getById(String id) throws SQLException, MyException;
    
    void UpdatePayment (Payment newPay) throws Exception;
}
