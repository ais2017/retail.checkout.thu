/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

/**
 *
 * @author admin
 */
public class ReturnableProduct {
    
    private  Product product;
    private  int quantity;
    
    public ReturnableProduct(Product rprod, int Quantity) throws MyException{
       
       if (rprod == null)
       {
           throw new MyException("Объект товар не может быть пустым!");
       } 
        
       if (Quantity <= 0)
       {
           throw new MyException("Количество товаров должно быть строго положительно!");
       } 
       
       this.product = rprod;
       this.quantity = Quantity;
    }
    
    public Product getProduct() {
       return this.product;
    } 
    public int getQuantity() {
       return this.quantity;
    } 
    
    public void AddQuantity(int addPoints) throws MyException {
       
       if (addPoints <= 0)
       {
           throw new MyException("Количество добавляемых товаров должно быть строго положительно!");
       } 
        this.quantity = this.quantity + addPoints;
    }
    
    public void ReduceQuantity(int cutPoints) throws MyException {
       
       if (cutPoints <= 0)
       {
           throw new MyException("Количество исключаемых из возврата товаров должно быть строго положительно!");
       } 
       if ((this.quantity - cutPoints) <= 0)
       {
           throw new MyException("Невозможно сократить количество товаров в возврате!");
       }
        this.quantity = this.quantity - cutPoints;
    }
    
}
