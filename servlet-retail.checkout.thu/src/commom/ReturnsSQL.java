package commom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ReturnsSQL implements Returns {

	Connection con = null;
	String conUrl = "jdbc:sqlserver://127.0.0.1; databaseName=retail_checkout_thu; user=SA; password=1Qwerty7;";
	Statement statement = null;
	Statement statement2 = null;
	Statement statement3 = null;
	
	public ReturnsSQL() throws SQLException {
		con = DriverManager.getConnection(conUrl);
		statement = con.createStatement();
		statement2 = con.createStatement();
		statement3 = con.createStatement();
	}

	@Override
	public void save(Return ret) throws SQLException {
		int status = 0;

		if (ret.getStatus()) status = 1;
		statement.executeUpdate("INSERT INTO ReturnTab VALUES ("  + ret.getID() + ", CONVERT( DATETIME, '" + ret.getDate() + "', 120), " + status + ");");
		for (Entry<String, ReturnableProduct> entry : ret.getObjectsOfReturn().entrySet())
			statement.executeUpdate("INSERT INTO ReturnableProduct VALUES ('" + entry.getValue().getProduct().getId() + "', " + ret.getID() + ", " + entry.getValue().getQuantity() + ");");
	}

	@Override
	public ArrayList<Return> ShowAllReturns() throws SQLException, MyException {
		ArrayList<Return> arrayList = new ArrayList<Return>();
		Map <String, ReturnableProduct> objectsOfReturn;
		ResultSet resSetReturns = statement.executeQuery("SELECT * FROM ReturnTab");
		ResultSet resSetSavedProducts;
		ResultSet resSetProducts;
		
		while (resSetReturns.next() != false) {
			objectsOfReturn = new HashMap<String, ReturnableProduct>();
			resSetSavedProducts = statement2.executeQuery("SELECT * FROM ReturnableProduct WHERE returnID = " + resSetReturns.getInt(1));
			while (resSetSavedProducts.next() != false) {
				resSetProducts = statement3.executeQuery("SELECT * FROM Product WHERE id LIKE '" + resSetSavedProducts.getString(1) + "';");
				if (resSetProducts.next() != false)
					objectsOfReturn.put(resSetSavedProducts.getString(1), new ReturnableProduct(new Product(resSetProducts.getString(1), resSetProducts.getBigDecimal(2), resSetProducts.getInt(3)), resSetSavedProducts.getInt(3)));
			}
			arrayList.add(new Return(resSetReturns.getInt(1), resSetReturns.getBoolean(3), resSetReturns.getDate(2),  objectsOfReturn));
		}
		return arrayList;
	}

	@Override
	public Return getById(Long id) throws SQLException, MyException {
		ResultSet resultSet = statement.executeQuery("SELECT * FROM ReturnTab WHERE returnID = " + id + ";");
		ResultSet resSetSavedProducts;
		ResultSet resSetProducts;
		
		Map <String, ReturnableProduct> objectsOfReturn = new HashMap<String, ReturnableProduct>();		
		
		if (resultSet.next() != false) {
			objectsOfReturn = new HashMap<String, ReturnableProduct>();
			resSetSavedProducts = statement2.executeQuery("SELECT * FROM ReturnableProduct WHERE returnID = " + resultSet.getInt(1) + ";");
			while (resSetSavedProducts.next() != false) {
				resSetProducts = statement3.executeQuery("SELECT * FROM Product WHERE id LIKE '" + resSetSavedProducts.getString(1) + "';");
				if (resSetProducts.next() != false)
					objectsOfReturn.put(resSetSavedProducts.getString(1), new ReturnableProduct(new Product(resSetProducts.getString(1), resSetProducts.getBigDecimal(2), resSetProducts.getInt(3)), resSetSavedProducts.getInt(3)));
			}
			return new Return(id.intValue(), resultSet.getBoolean(3), resultSet.getDate(2), objectsOfReturn);
		}
		return null;
	}

	@Override
	public void UpdateReturn(Return newReturn, Long Id) throws Exception {
		
		short status = 0;
	
	if (newReturn.getStatus()) status = 1;
	statement.executeUpdate("UPDATE  ReturnTab SET status = " + status + " WHERE returnID = " + newReturn.getID() + ";");
	
	for (Entry<String, ReturnableProduct> entry : newReturn.getObjectsOfReturn().entrySet())
		try {
			statement.executeUpdate("INSERT INTO ReturnableProduct VALUES ('" + entry.getValue().getProduct().getId() + "', " + newReturn.getID() + ", " + entry.getValue().getQuantity() + ");");
		} catch (SQLException e) {
			// TODO: handle exception
		}

	}

}
