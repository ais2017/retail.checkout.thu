/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commom;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Products {
    public void CreateProductList () throws Exception;

    public ArrayList<Product> ShowAllProducts() throws MyException, SQLException;

    public Product getById(String id) throws SQLException, MyException;
    
    public void UpdateProduct(Product newProduct, String Id) throws Exception;
}
