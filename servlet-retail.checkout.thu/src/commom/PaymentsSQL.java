package commom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class PaymentsSQL implements Payments {

	Connection con = null;
	String conUrl = "jdbc:sqlserver://127.0.0.1; databaseName=retail_checkout_thu; user=SA; password=1Qwerty7;";
	Statement statement = null;
	Statement statement2 = null;
	Statement statement3 = null;
	
	public PaymentsSQL() throws SQLException {
		con = DriverManager.getConnection(conUrl);
		statement = con.createStatement();
		statement2 = con.createStatement();
		statement3 = con.createStatement();
	} 

	@Override
	public void save(Payment pay) throws SQLException {
		statement.executeUpdate("INSERT INTO Payment VALUES ('" + pay.getIdPay() + "', '" + pay.getMethod() + "', 0, CONVERT( DATETIME, '" + pay.getDate() + "', 120), " + pay.getObjectOfPayment().getID() + ")");
	}

	@Override
	public ArrayList<Payment> ShowAllPayments() throws SQLException {
//		ArrayList<Payment> arrayList = new ArrayList<Payment>();
//		ResultSet resultSet = statement.executeQuery("SELECT * FROM Card");
//		
//		while (resultSet.next() != false) {
//			//arrayList.add(new Payment(resultSet.getString(1), resultSet.getInt(2)));
//		}
//		return arrayList;
		return null;
	}

	@Override
	public Payment getById(String id) throws SQLException, MyException {
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Payment WHERE idPay LIKE '" + id + "';");
		ResultSet resSetSavedProducts;
		ResultSet resSetProducts;		
		Map <String, SelectedProduct> sprods = new HashMap<String, SelectedProduct>();		
		
		if (resultSet.next() != false) {
			sprods = new HashMap<String, SelectedProduct>();
			resSetSavedProducts = statement2.executeQuery("SELECT * FROM SelectedProduct WHERE purchaseID = " + resultSet.getInt(1) + ";");
			while (resSetSavedProducts.next() != false) {
				resSetProducts = statement3.executeQuery("SELECT * FROM Product WHERE id LIKE '" + resSetSavedProducts.getString(1) + "';");
				if (resSetProducts.next() != false)
					sprods.put(resSetSavedProducts.getString(1), new SelectedProduct(new Product(resSetProducts.getString(1), resSetProducts.getBigDecimal(2), resSetProducts.getInt(3)), resSetSavedProducts.getInt(3)));
			}
			return new Payment(id, resultSet.getString(2), resultSet.getBoolean(3), resultSet.getDate(4), new Purchase(resultSet.getInt(5), sprods));
		}
		return null;
	}

	@Override
	public void UpdatePayment(Payment newPay) throws Exception {
		int status = 0;
		
		if (newPay.getStatus()) status = 1;
		statement.executeUpdate("UPDATE Payment SET status = " + status + " WHERE idPay LIKE '" + newPay.getIdPay() + "'");
	}

}
