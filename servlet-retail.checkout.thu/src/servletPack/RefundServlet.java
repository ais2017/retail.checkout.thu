package servletPack;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commom.CardsSQL;
import commom.PaymentsSQL;
import commom.ProductsSQL;
import commom.PurchaseSQL;
import commom.ReturnsSQL;
import commom.Scenario;
import commom.StockImpl;

/**
 * Servlet implementation class refundServlet
 */
@WebServlet("/RefundServlet")
public class RefundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private boolean flag = true;
    private Integer ID;
    Scenario scenario;
       
    /**
     * @throws SQLException 
     * @throws ClassNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public RefundServlet() throws SQLException, ClassNotFoundException {
        super();
	    PurchaseSQL purchs = new PurchaseSQL();
	    ReturnsSQL rets = new ReturnsSQL();
	    PaymentsSQL pays = new PaymentsSQL();
	    CardsSQL cards = new CardsSQL();
	    ProductsSQL products = new ProductsSQL();
	    StockImpl stock = new StockImpl(products);
        scenario = new Scenario(purchs, rets, pays, cards, products, stock);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("sdfg");
        String redir = "return.jsp";
	    	
		try {
	    	switch (Integer.parseInt(request.getParameter("todo"))) {
			case 0:
				if (flag) {
					ID = new Integer(request.getParameter("purchID"));
					scenario.CreateReturn(ID.longValue(), request.getParameter("prodID"), new Integer(request.getParameter("prodQuant")));
					flag = false;
				}
				else {
					scenario.AddItemToReturn(ID.longValue(), ID.longValue(), request.getParameter("prodID"), new Integer(request.getParameter("prodQuant")));
				}
				break;
			case 1:
				scenario.CloseReturn(ID.longValue());
				flag = true;
				redir = "index.jsp";
				break;
	
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        response.sendRedirect(redir);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
