package servletPack;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commom.*;


/**
 * Servlet implementation class SaleServlet
 */
@WebServlet("/SaleServlet")
public class SaleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private boolean flag = true;
    private Integer ID = 0;
    Scenario scenario;
    
    /**
     * @throws SQLException 
     * @throws NamingException 
     * @throws ClassNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public SaleServlet() throws SQLException, NamingException, ClassNotFoundException {
        super();
	    PurchaseSQL purchs = new PurchaseSQL();
	    ReturnsSQL rets = new ReturnsSQL();
	    PaymentsSQL pays = new PaymentsSQL();
	    CardsSQL cards = new CardsSQL();
	    ProductsSQL products = new ProductsSQL();
	    StockImpl stock = new StockImpl(products);
        scenario = new Scenario(purchs, rets, pays, cards, products, stock);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("sdfg");
        String redir = "sale.jsp";
	    	
		try {
	    	switch (Integer.parseInt(request.getParameter("todo"))) {
			case 0:
				if (flag) {
					scenario.CreatePurchase(ID, request.getParameter("prodID"), new Integer(request.getParameter("prodQuant")));
					flag = false;
				}
				else {
					scenario.AddItemToPurchase(ID.longValue(), request.getParameter("prodID"), new Integer(request.getParameter("prodQuant")));
				}
				break;
			case 1:
				scenario.addCardToPurchase(ID.longValue(), request.getParameter("cardID"), new Integer(request.getParameter("ballsValue")));
				break;
			case 2:
				scenario.addPayment(ID.longValue(), ID.toString(), "Card");
				ID++;
				flag = true;
				redir = "index.jsp";
				break;
			case 3:
				scenario.addPayment(ID.longValue(), ID.toString(), "Cache");
				ID++;
				flag = true;
				redir = "index.jsp";
				break;
	
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        response.sendRedirect(redir);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
