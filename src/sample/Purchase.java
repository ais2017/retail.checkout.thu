/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;
import java.math.BigDecimal;

/**
 *
 * @author admin
 */
public class Purchase {
    
    private  Map <String, SelectedProduct> sprods;
    private  Card card;
    private  int cutPoints;
    private  Payment payment;
    private  boolean status;
    
    public Purchase (Map <String, SelectedProduct> prodList) throws MyException {
       if (prodList == null)
       {
           throw new MyException("Список товаров не может быть пустым!");
       }
       this.sprods = prodList;
       this.status = false;
    }
    
     public Map <String, SelectedProduct> getList() {
       return this.sprods;
    } 
    
    public Card getCard() throws MyException {
       if (this.card == null)
       {
           throw new MyException("Карта не прикреплена!");
       } 
       else
           return this.card;
    } 
    
    public int getCutPoints() {
       return this.cutPoints;
    } 
    
    public Payment getPayment() {
       return this.payment;
    } 
    
    public boolean getStatus() {
       return this.status;
    } 
    
    public void addProduct(SelectedProduct addprod) throws MyException{
    
        String key = addprod.getProduct().getId();
        SelectedProduct findingProd = sprods.get(key);
        if (findingProd == null){
            sprods.put(key, addprod);
        }
        else {
            int addPoints = findingProd.getQuantity();
            addprod.AddQuantity(addPoints);
            sprods.put(key, addprod);
        }
    }
    
    public void deleteProduct(SelectedProduct delprod) throws MyException{
        String key = delprod.getProduct().getId();
        SelectedProduct findingProd = sprods.get(key);
        if (findingProd == null){
            throw new MyException("Операция невозможна! Исключаемый товар в покупке не найден!");
        }
        else {
            if (findingProd.getQuantity() > delprod.getQuantity()) {
                int delPoints = delprod.getQuantity();
                findingProd.ReduceQuantity(delPoints);
                sprods.put(key, findingProd);
            } else if (findingProd.getQuantity() == delprod.getQuantity()) {
                for(Iterator<Map.Entry<String, SelectedProduct>> it = sprods.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<String, SelectedProduct> entry = it.next();
                    if(entry.getKey().equals(key)) {
                            it.remove();
                    }
                }
            }
            else {
                throw new MyException("Операция невозможна! Количество исключаемого товара превышает количество товара в покупке!");
            }
        }
    }
    
    public void setCard(Card addCard) throws MyException{
        if (addCard == null)
        {
           throw new MyException("Невозможно выполнить операцию! Объект Карта пустой");
        } 
        this.card = addCard;
    }
    
    public void setPayment(Payment pay) throws MyException{
        if (pay == null)
        {
           throw new MyException("Невозможно выполнить операцию! Объект Оплата покупки пустой");
        } 
        this.payment = pay;
    }
    
     public void closePurchase() throws MyException {
       
       if (this.status == true)
       {
           throw new MyException("Покупка уже закрыта!");
       } 
       this.status = true;
    }
    
    public BigDecimal SummOfPurchase () {
        BigDecimal res = new BigDecimal("0.00");  
        BigDecimal adds, mn, sumadds, result; 
        int mul;
        Iterator<Map.Entry<String, SelectedProduct>> entries = sprods.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, SelectedProduct> entry = entries.next();
            adds = entry.getValue().getProduct().getPrice();
            mul = entry.getValue().getQuantity();
            mn = new BigDecimal(Integer.toString(mul));
            sumadds = adds.multiply(mn);
            result = res.add(sumadds);
            res = result;
        }
        return res;
    }
    
    public void PaymentWithPoints (int points) throws MyException{
        if (this.card == null) {
            throw new MyException("Невозможно выполнить операцию! Карта не прикреплена");
        }
        else {
            if (this.card.getQuantity() < points) {
                throw new MyException("Невозможно выполнить операцию! Недостаточно баллов на карте");            
            }
            else {
                this.card.ReduceQuantity(points);
                this.cutPoints = points;
            }
        }
    }
}
