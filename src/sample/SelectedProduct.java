/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

/**
 *
 * @author admin
 */
public class SelectedProduct {
  
    private  Product product;
    private  int quantity;
    public SelectedProduct(Product sprod, int Quantity) throws MyException{
       
       if (sprod == null)
       {
           throw new MyException("Объект товар не может быть пустым!");
       } 
        
       if (Quantity <= 0)
       {
           throw new MyException("Количество товаров должно быть строго положительно!");
       } 
       
       this.product = sprod;
       this.quantity = Quantity;
    }
    
    public Product getProduct() {
       return this.product;
    } 
    public int getQuantity() {
       return this.quantity;
    } 
    
    public void AddQuantity(int addPoints) throws MyException {
       
       if (addPoints <= 0)
       {
           throw new MyException("Количество добавляемых товаров должно быть строго положительно!");
       } 
        this.quantity = this.quantity + addPoints;
    }
    
    public void ReduceQuantity(int cutPoints) throws MyException {
       
       if (cutPoints <= 0)
       {
           throw new MyException("Количество удаляемых из покупки товаров должно быть строго положительно!");
       } 
       if ((this.quantity - cutPoints) <= 0)
       {
           throw new MyException("Невозможно сократить количество товаров в покупке!");
       }
        this.quantity = this.quantity - cutPoints;
    }
    
}

