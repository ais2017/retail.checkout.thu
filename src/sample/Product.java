/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;

/**
 *
 * @author admin
 */

public class Product {
    private  String id;
    private  BigDecimal price;
    private  int amount;
    public Product(String Id, BigDecimal Price, int Amount) throws MyException{
       
       if ((Id == null) || (Id == ""))
       {
           throw new MyException("Штрих-код не может быть пустым!");
       } 
        
       if (Price.compareTo(new BigDecimal("0.00")) <= 0)
       {
           throw new MyException("Цена товара - строго положительное число!");
       } 
       
       if (Amount < 0)
       {
           throw new MyException("Число товаров должно быть неотрицательно!");
       }
       this.id = Id;
       this.price = Price;
       this.amount = Amount;
    }
    
    public String getId() {
       return this.id;
    } 
    public BigDecimal getPrice() {
       return this.price;
    } 
    public int getAmount() {
       return this.amount;
    } 
    
    /*public void setPrice(BigDecimal newPrice) throws MyException {
        if (newPrice.compareTo(new BigDecimal("0.00")) <= 0)
        {
           throw new MyException("Цена товара - строго положительное число!");
        } 
        this.price = newPrice;
    }*/
}


