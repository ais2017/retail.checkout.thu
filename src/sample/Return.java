/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;
import java.text.*;

/**
 *
 * @author admin
 */
public class Return {
    private  boolean status;
    private  Date date;
    private  Map <String, ReturnableProduct> objectsOfReturn;
    public Return(Map <String, ReturnableProduct> objects) throws MyException{
        
       if (objects == null)
       {
           throw new MyException("Список возврата не может быть пустым!");
       } 
      
       this.status = false;
       this.date = new Date();
       this.objectsOfReturn = objects;
    }
    
    public boolean getStatus() {
       return this.status;
    } 
    
    public String getDate() {
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.date);
       return rDate;
    } 
    
    public Map <String, ReturnableProduct> getObjectsOfReturn() {
       return this.objectsOfReturn;
    } 
    
    public void addProductToReturn(ReturnableProduct addProd) throws MyException{
    
        String key = addProd.getProduct().getId();
        ReturnableProduct findingProd = objectsOfReturn.get(key);
        if (findingProd == null){
            objectsOfReturn.put(key, addProd);
        }
        else {
            int addPoints = findingProd.getQuantity();
            addProd.AddQuantity(addPoints);
            objectsOfReturn.put(key, addProd);
        }
    }
    
    public void deleteProductFromReturn(ReturnableProduct delProd) throws MyException{
        String key = delProd.getProduct().getId();
        ReturnableProduct findingProd = objectsOfReturn.get(key);
        if (findingProd == null){
            throw new MyException("Операция невозможна! Исключаемый товар в возврате не найден!");
        }
        else {
            if (findingProd.getQuantity() > delProd.getQuantity()) {
                int delPoints = delProd.getQuantity();
                findingProd.ReduceQuantity(delPoints);
                objectsOfReturn.put(key, findingProd);
            } else if (findingProd.getQuantity() == delProd.getQuantity()) {
                for(Iterator<Map.Entry<String, ReturnableProduct>> it = objectsOfReturn.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<String, ReturnableProduct> entry = it.next();
                    if(entry.getKey().equals(key)) {
                            it.remove();
                    }
                }
            }
            else {
                throw new MyException("Операция невозможна! Количество исключаемого товара превышает количество товара в возврате!");
            }
        }
    }
    
   
    public void confirmReturn() throws MyException {
       
       if (this.status == true)
       {
           throw new MyException("Операция возврата завершена!");
       } 
        this.status = true;
    }
    
    public void rejectReturn() throws MyException {
       
       if (this.status == false)
       {
           throw new MyException("Возрат отклонен!");
       } 
        this.status = false;
    }
}
