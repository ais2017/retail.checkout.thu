/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Cards {
    public void CreateCardList () throws Exception;

    public ArrayList<Card> ShowAllCards();

    public Card getById(String id);
    
    void UpdateCard (Card newCard) throws Exception;
}
