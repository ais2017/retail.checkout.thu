/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author admin
 */
public class PurchasesImpl implements Purchases {
    
    private AtomicLong currentOrderId = new AtomicLong(0);
    
    public Long incrementAndGetId() {
        return currentOrderId.incrementAndGet();
    }

    public static Map<Long, Purchase> purchases = new HashMap<>();

    @Override
    public void save(Purchase purch) {
        Long Id = incrementAndGetId();
        purchases.put(Id, purch);
    }

    @Override
    public Purchase getById(Long id) {
        return purchases.get(id);
    }

    @Override
    public ArrayList<Purchase> ShowAllPurchases () {
        ArrayList ListOfPurchases = new ArrayList<>(purchases.entrySet());
        return ListOfPurchases;
    }
    
    @Override
    public void UpdatePurchase(Purchase newPurch, Long Id) throws Exception {
        if (getById(Id)!=null) {
            purchases.put(Id, newPurch);
        }
        else
            throw new Exception();

    }

}
