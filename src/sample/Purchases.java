/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Purchases {
    public void save(Purchase purchase);

    public ArrayList<Purchase> ShowAllPurchases();

    public Purchase getById(Long id);
    
    void UpdatePurchase (Purchase newPurch, Long Id) throws Exception;
    
}
