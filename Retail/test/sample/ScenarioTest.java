/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class ScenarioTest {
    Scenario scenario;
    Purchase purch;
    Return ret;
    
    public ScenarioTest() {
    }
    
    @Before
    public void setUp() throws Exception{
       PurchasesImpl purchs = new PurchasesImpl();
       ReturnsImpl rets = new ReturnsImpl();
       PaymentsImpl pays = new PaymentsImpl();
       CardsImpl cards = new CardsImpl();
       ProductsImpl products = new ProductsImpl();
       StockImpl stock = new StockImpl(products);
       scenario = new Scenario(purchs, rets, pays, cards, products, stock);
       scenario.productsImpl.CreateProductList();
       scenario.cardsImpl.CreateCardList();
       scenario.CreatePurchase("7 209380 644345",5);
       Long k = 1l;
       Long k2 = 2l;
       Card addCard = new Card("1234 5678 7777 2378", 500);
       scenario.AddItemToPurchase(k,"5 416470 090014",1);
       Purchase puch = scenario.purchasesImpl.getById(k);
       puch.closePurchase();
       scenario.purchasesImpl.UpdatePurchase(puch, k);
       scenario.CreatePurchase("2 670500 090014",7);
       Purchase puch2 = scenario.purchasesImpl.getById(k2);
       puch2.setCard(addCard);
       scenario.purchasesImpl.UpdatePurchase(puch2, k2);
       scenario.CreatePurchase("8 410545 090014",10);
       scenario.CreateReturn(k,"7 209380 644345",3);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of CreatePurchase method, of class Scenario.
     */
    @Test
    public void testCreatePurchase1() throws Exception {
        System.out.println("testCreatePurchase1");
        try {
           scenario.CreatePurchase("7 9862210 742018",20);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Товара с таким ID не существует!", ex.getMessage());
        }
    }
    
    @Test
    public void testCreatePurchase2() throws Exception {
        System.out.println("testCreatePurchase2");
        try {
           scenario.CreatePurchase("6 410500 090014",40);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Требуемого количества товара нет!", ex.getMessage());
        }
    }
    
    @Test
    public void testCreatePurchase3() throws Exception {
        System.out.println("testCreatePurchase3");
        BigDecimal result = scenario.CreatePurchase("4 607099 091450",5);
        Assert.assertEquals(4, scenario.purchasesImpl.ShowAllPurchases().size());
        Long i = 4l;
        Purchase purch = scenario.purchasesImpl.getById(i);
        Assert.assertNotEquals(null, purch);
        BigDecimal summ = new BigDecimal("477.50");
        assertEquals(summ, result);
    }

    /**
     * Test of AddItemToPurchase method, of class Scenario.
    */
    
    @Test
    public void testAddItemToPurchase1() throws Exception {
        System.out.println("testAddItemToPurchase1");
        Long key = 7l;
        try {
           scenario.AddItemToPurchase(key,"4 607099 091450",5);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Покупки с таким ID не существует!", ex.getMessage());
        }
    }
    
    @Test
    public void testAddItemToPurchase2() throws Exception {
        System.out.println("testAddItemToPurchase2");
        Long key = 1l;
        try {
           scenario.AddItemToPurchase(key,"4 607099 091450",5);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Покупка уже оплачена! Добавить товар невозможно.", ex.getMessage());
        }
    }
    
    @Test
    public void testAddItemToPurchase3() throws Exception {
        System.out.println("testAddItemToPurchase3");
        Long key = 2l;
        try {
           scenario.AddItemToPurchase(key,"4 606799 091450",5);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Товара с таким ID не существует!", ex.getMessage());
        }
    }
    
    @Test
    public void testAddItemToPurchase4() throws Exception {
        System.out.println("testAddItemToPurchase4");
        Long key = 2l;
        try {
           scenario.AddItemToPurchase(key,"3 410500 445614",120);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Требуемого количества товара нет!", ex.getMessage());
        }
    }
    
    @Test
    public void testAddItemToPurchase5() throws Exception {
        System.out.println("testAddItemToPurchase5");
        Long key = 2l;
        String IdProd = "3 410500 445614";
        BigDecimal result = scenario.AddItemToPurchase(key, IdProd, 5);
        boolean find = scenario.purchasesImpl.getById(key).getList().containsKey(IdProd);
        assertEquals(find, true);
        BigDecimal summ = new BigDecimal("8579.00");
        assertEquals(summ, result);
    }

    /**
     * Test of addCardToPurchase method, of class Scenario.
    */
    
    @Test
    public void testAddCardToPurchase1() throws Exception {
        System.out.println("testAddCardToPurchase1");
        Long key = 1l;
        try {
           scenario.addCardToPurchase(key,"3534 5678 7777 2378",50);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Карты или покупки с таким ID не существует!", ex.getMessage());
        }
    }
    
    @Test
    public void testAddCardToPurchase2() throws Exception {
        System.out.println("testAddCardToPurchase2");
        Long key = 1l;
        try {
           scenario.addCardToPurchase(key,"1234 5678 7777 2378",50);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Покупка уже оплачена! Карту прикрепить невозможно.", ex.getMessage());
        }
    }
    
    @Test
    public void testAddCardToPurchase3() throws Exception {
        System.out.println("testAddCardToPurchase3");
        Long key = 2l;
        try {
           scenario.addCardToPurchase(key,"1234 5678 7777 2378",50);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Карта уже прикреплена!", ex.getMessage());
        }
    }
    
    @Test
    public void testAddCardToPurchase4() throws Exception {
        System.out.println("testAddCardToPurchase4");
        Long key = 3l;
        try {
           scenario.addCardToPurchase(key,"1234 5678 7777 2378",100);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Невозможно выполнить операцию! Недостаточно баллов на карте", ex.getMessage());
        }
    }
    
    @Test
    public void testAddCardToPurchase5() throws Exception {   
        System.out.println("testAddCardToPurchase5");
        Long key = 3l;
        scenario.addCardToPurchase(key,"1234 5678 7777 2378", 10);
        Card find = scenario.purchasesImpl.getById(key).getCard();
        Assert.assertNotEquals(find, null);
    }
   
    /**
     * Test of addPayment method, of class Scenario.
    */
    
    @Test
    public void testaddPayment1() throws Exception {
        System.out.println("testaddPayment1");
        Long key = 1l;
        try {
           scenario.addPayment(key,"1","Cash");
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Покупка уже оплачена!", ex.getMessage());
        }
    }
    
    @Test
    public void testaddPayment2() throws Exception {
        System.out.println("testaddPayment2");
        Long key = 2l;
        String kpay = "2";
        scenario.addPayment(key,kpay,"Cash");
        Payment pay = scenario.paymentsImpl.getById(kpay);
        Purchase purchpay = scenario.purchasesImpl.getById(key);
        boolean result1 = pay.getStatus();
        boolean result2 = purchpay.getStatus();
        assertEquals(result1, true);   
        assertEquals(result2, true);
        int res = scenario.productsImpl.getById("2 670500 090014").getAmount();
        assertEquals(res, 293);
    }
    
    /**
     * Test of CreateReturn method, of class Scenario.
    */
    
    @Test
    public void testCreateReturn1() throws Exception {
        System.out.println("testCreateReturn1");
        Long key = 3l;
        try {
           scenario.CreateReturn(key, "6 410500 090014",40);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Покупка еще не оплачена! Возврат невозможен.", ex.getMessage());
        }
    }
    
    @Test
    public void testCreateReturn2() throws Exception {
        System.out.println("testCreateReturn2");
        Long key = 1l;
        try {
           scenario.CreateReturn(key, "8 410545 090014",40);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Товара с таким ID в покупке нет!", ex.getMessage());
        }
    }

    @Test
    public void testCreateReturn3() throws Exception {
        System.out.println("testCreateReturn3");
        Long key = 1l;
        scenario.CreateReturn(key, "7 209380 644345",4);
        Assert.assertEquals(2, scenario.returnsImpl.ShowAllReturns().size());
        Long i = 2l;
        Return ret = scenario.returnsImpl.getById(i);
        Assert.assertNotEquals(null, ret);
    }
    
    /**
     * Test of AddItemToReturn method, of class Scenario.
    */
    
    @Test
    public void testAddItemToReturn() throws Exception {
        System.out.println("testAddItemToReturn");
        Long kp = 1l;
        Long kr = 1l;
        String IdP ="7 209380 644345";
        scenario.AddItemToReturn(kp,kr,IdP,1);
        boolean find = scenario.returnsImpl.getById(kr).getObjectsOfReturn().containsKey(IdP);
        assertEquals(find, true);
        int result = scenario.returnsImpl.getById(kr).getObjectsOfReturn().get(IdP).getQuantity();
         assertEquals(result, 4);    
    }
    
    @Test
    public void testCloseReturn1 () throws Exception {
        System.out.println("testCloseReturn1");
        Long key = 9l;
        try {
           scenario.CloseReturn(key);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Возврата с таким ID не существует!", ex.getMessage());
        }
    }
    
    @Test
    public void testCloseReturn2 () throws Exception {
        System.out.println("testCloseReturn2");
        Long key = 1l;
        Return ret = scenario.returnsImpl.getById(key);
        ret.confirmReturn();
        try {
           scenario.CloseReturn(key);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertEquals("Операция возврата завершена!", ex.getMessage());
        }
    }
    
}
