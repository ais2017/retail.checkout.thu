/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class ReturnTest {
    public Product prod1; 
    public ReturnableProduct rprod1; 
    public Return ret;
    Map <String, ReturnableProduct> pList;
    
    public ReturnTest() {
    }
    
    @Before
    public void setUp() throws MyException {
        prod1 = new Product("4 607099 091450", new BigDecimal("95.5"), 50);
        rprod1 = new ReturnableProduct(prod1, 2);
        pList = new HashMap();
        pList.put(rprod1.getProduct().getId(), rprod1);
        ret = new Return(pList);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Construction of class Return.
     */
    @Test
    public void testConstructionFailsIfObjectsListIsNull() throws MyException {
        System.out.println("ConstructionFailsIfObjectsListIsNull");
        try {
            new Return(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    /**
     * Test of AddProductToReturn method, of class Return.
     */
    @Test
    public void testAddProductToReturn1() throws MyException {
        System.out.println("testAddProductToReturn1");
        ReturnableProduct addProd;
        addProd = new ReturnableProduct(prod1, 2);
        String key = addProd.getProduct().getId();
        int quant = ret.getObjectsOfReturn().get(key).getQuantity() + addProd.getQuantity();
        ret.addProductToReturn(addProd);
        ReturnableProduct fProd = ret.getObjectsOfReturn().get(key);
        int result = fProd.getQuantity();
        assertEquals(quant, result);
    }
    
    @Test
    public void testAddProductToReturn2() throws MyException {
        System.out.println("testAddProductToReturn2");
        Product prd;
        ReturnableProduct addProd;
        prd = new Product("4 607085 860060", new BigDecimal("70.00"), 30);
        addProd = new ReturnableProduct(prd, 1);
        ret.addProductToReturn(addProd);
        String key = addProd.getProduct().getId();
        ReturnableProduct result = ret.getObjectsOfReturn().get(key);
        assertEquals(addProd, result);
    }
    
     /**
     * Test of deleteProductFromReturn method, of class Return.
     */
    @Test
    public void testdeleteProductFromReturn1() throws MyException {
        System.out.println("deleteProductFromReturn1");
        ReturnableProduct delProd;
        delProd = new ReturnableProduct(new Product("4 607099 000000", new BigDecimal("95.5"), 50), 2);
        try {
            ret.deleteProductFromReturn(delProd);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void deleteProductFromReturn2() throws MyException {
        System.out.println("deleteProductFromReturn2");
        ReturnableProduct delProd;
        delProd = new ReturnableProduct(prod1, 5);
        try {
            ret.deleteProductFromReturn(delProd);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void deleteProductFromReturn3() throws MyException {
        System.out.println("deleteProductFromReturn3");
        ReturnableProduct delProd;
        delProd = new ReturnableProduct(prod1, 2);
        ret.deleteProductFromReturn(delProd);
        String key = delProd.getProduct().getId();
        ReturnableProduct result = ret.getObjectsOfReturn().get(key);
        assertEquals(result, null);
    }
    
    /**
     * Test of confirmReturn method, of class Return.
     */
    @Test
    public void testConfirmReturn() throws Exception {
        ret.confirmReturn();
        assertEquals(ret.getStatus(), true);
    }


}
