/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author admin
 */
public class ProductTest extends Assert{
    public Product prod;
    
    public ProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Construction of class Product.
     */
    @Test
    public void testConstructionFailsIfIdIsNull() throws MyException {
        System.out.println("ConstructionFailsIfIdIsNull");
        BigDecimal x = new BigDecimal("45.5");
        try {
            new Product(null, x, 10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfIdIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfIdIsEmpty");
        BigDecimal x = new BigDecimal("45.5");
        try {
           new Product("", x, 20);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
    @Test
    public void testConstructionFailsIfPriceIsNegative() throws MyException {
        System.out.println("ConstructionFailsIfPriceIsNegative");
        BigDecimal x = new BigDecimal("-45.5");
        try {
            new Product("7 209380 754841", x, 10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
    @Test
    public void testConstructionFailsIfAmountIsNegative() throws MyException {
        System.out.println("ConstructionFailsIfAmountIsNegative");
        BigDecimal x = new BigDecimal("45.5");
        try {
            new Product("7 209380 754841", x, -10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }

    @Test
    public void testAfterConstructionId() throws MyException{
       System.out.println("testAfterConstructionId");
       String Id = "5 201360 644841";
       prod = new Product(Id, new BigDecimal("100.5"), 10);
       assertEquals(Id, prod.getId());
    }

    @Test
    public void testAfterConstructionPrice() throws MyException{
       System.out.println("testAfterConstructionPrice");
       BigDecimal Price = new BigDecimal("45.5");
       prod = new Product("7 209380 644841", Price, 20);
       assertEquals(Price, prod.getPrice());
    }
   
    @Test
    public void testAfterConstructionAmount() throws MyException{
       System.out.println("testAfterConstructionAmount");
       int Amount = 50;
       prod = new Product("7 209380 644345", new BigDecimal("500.5"), Amount);
       assertEquals(Amount, prod.getAmount());
    }
    
    /**
     * Test of setPrice method, of class Product.
     
     @Test
    public void testSetPriceIfPriceIsNull() throws MyException{
       System.out.println("testSetPriceIfPriceIsNull");
       BigDecimal newPrice = new BigDecimal("0.00");
       prod = new Product("7 209380 644547", new BigDecimal("500.5"), 30);
       try {
          prod.setPrice(newPrice);
          Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testSetPrice() throws MyException{
       System.out.println("testSetPrice");
       BigDecimal newPrice = new BigDecimal("90.0");
       prod = new Product("7 209380 644547", new BigDecimal("500.5"), 30);
       prod.setPrice(newPrice);
       assertEquals(newPrice, prod.getPrice());
    }
    */
}
