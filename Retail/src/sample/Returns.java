/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Returns {
    
    public void save(Return ret);

    public ArrayList<Return> ShowAllReturns();

    public Return getById(Long id);
    
    void UpdateReturn (Return newReturn, Long Id) throws Exception;
}
