/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

/**
 *
 * @author admin
 */
public class Card {
    private  String id_card;
    private  int quantity;
    public Card(String Id, int Quantity) throws MyException{
       
       if ((Id == null) || (Id == ""))
       {
           throw new MyException("Номер карты не может быть пустым!");
       } 
        
       if (Quantity < 0)
       {
           throw new MyException("Количество баллов должно быть неотрицательно!");
       } 
       
       this.id_card = Id;
       this.quantity = Quantity;
    }
    
    public String getId() {
       return this.id_card;
    } 
    public int getQuantity() {
       return this.quantity;
    } 
       
    public void AddQuantity(int addPoints) throws MyException {
       
       if (addPoints <= 0)
       {
           throw new MyException("Количество добавляемых баллов должно быть строго положительно!");
       } 
        this.quantity = this.quantity + addPoints;
    }
    
    public void ReduceQuantity(int cutPoints) throws MyException {
       
       if (cutPoints <= 0)
       {
           throw new MyException("Количество списываемых баллов должно быть строго положительно!");
       } 
       if ((this.quantity - cutPoints) < 0)
       {
           throw new MyException("Невозможно выполнить операцию. Недостаточно баллов на карте!");
       }
        this.quantity = this.quantity - cutPoints;
    }
    
       
}
