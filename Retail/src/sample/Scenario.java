/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author admin
 */
public class Scenario {
    public Purchases purchasesImpl;
    public Returns returnsImpl;
    public Payments paymentsImpl;
    public Cards cardsImpl;
    public Products productsImpl;
    public Stock stockImpl;
        
    public Scenario (Purchases purchs, Returns rets, Payments pays, Cards cards, Products products, Stock stock){
        purchasesImpl = purchs;
        returnsImpl = rets;
        paymentsImpl = pays;
        cardsImpl = cards;
        productsImpl = products;
        stockImpl = stock;
    }
    
    public BigDecimal CreatePurchase(String IdProd, int count) throws Exception {
        Product prodInPurchase = productsImpl.getById(IdProd);
        if (prodInPurchase == null) {
            throw new MyException("Товара с таким ID не существует!");
        }
        if (prodInPurchase.getAmount() < count) {
            throw new MyException("Требуемого количества товара нет!");
        }
        SelectedProduct sprod = new SelectedProduct(prodInPurchase, count);
        Map <String, SelectedProduct> pList = new HashMap();
        pList.put(IdProd, sprod);
        Purchase newPurchase = new Purchase(pList);
        purchasesImpl.save(newPurchase);
        BigDecimal result = newPurchase.SummOfPurchase();
        return(result);
    }
    
      public BigDecimal AddItemToPurchase(Long IdPurch, String IdProd, int count) throws Exception {
        Purchase puch = purchasesImpl.getById(IdPurch);
        if (puch == null)
        {
            throw new MyException("Покупки с таким ID не существует!");
        }
        if (puch.getStatus())
        {
            throw new MyException("Покупка уже оплачена! Добавить товар невозможно.");
        }
        Product newItem = productsImpl.getById(IdProd);
        if (newItem == null) {
            throw new MyException("Товара с таким ID не существует!");
        }
        if (newItem.getAmount() < count) {
            throw new MyException("Требуемого количества товара нет!");
        }
        SelectedProduct SelectNewItem = new SelectedProduct(newItem, count);
        puch.addProduct(SelectNewItem);
        purchasesImpl.UpdatePurchase(puch, IdPurch);
        BigDecimal result = puch.SummOfPurchase();
        return(result);
    }
    
    public void addCardToPurchase(Long idPurchase, String idCard, int redPoints) throws Exception {
        Card attachCard = cardsImpl.getById(idCard);
        Purchase apuch = purchasesImpl.getById(idPurchase);
        if (attachCard == null || apuch == null)
        {
            throw new MyException("Карты или покупки с таким ID не существует!");
        }
        if (apuch.getStatus() == true)
        {
            throw new MyException("Покупка уже оплачена! Карту прикрепить невозможно.");
        }
        if (apuch.getCard() != null){
            throw new MyException("Карта уже прикреплена!");
        }
        if (attachCard.getQuantity() < redPoints)
        {
            throw new MyException("Невозможно выполнить операцию! Недостаточно баллов на карте");
        }
        apuch.setCard(attachCard);
        purchasesImpl.UpdatePurchase(apuch,idPurchase);
        Purchase apuch1 = purchasesImpl.getById(idPurchase);
        apuch1.PaymentWithPoints(redPoints);
        purchasesImpl.UpdatePurchase(apuch1,idPurchase);
        Card newCard = purchasesImpl.getById(idPurchase).getCard();
        cardsImpl.UpdateCard(newCard);
    }
    
    public void addPayment(Long idPurchase, String idPay, String Met) throws Exception {
        Purchase paypuch = purchasesImpl.getById(idPurchase);
        Payment pay = paymentsImpl.getById(idPay);
        if (pay != null)
        {
            throw new MyException("Оплата уже существует!");
        }
        if (paypuch.getStatus() == true)
        {
            throw new MyException("Покупка уже оплачена!");
        }
        
        Payment paym = new Payment(idPay,Met,paypuch);
        paymentsImpl.save(paym);
        paypuch.closePurchase();
        purchasesImpl.UpdatePurchase(paypuch,idPurchase);
        paym.confirmPayment();
        paymentsImpl.UpdatePayment(paym);
        Map <String, SelectedProduct> listOfProds = purchasesImpl.getById(idPurchase).getList();
        Iterator<Map.Entry<String, SelectedProduct>> entries = listOfProds.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, SelectedProduct> entry = entries.next();
            String IdProd = entry.getValue().getProduct().getId();
            int decQuan = entry.getValue().getQuantity();
            stockImpl.DecreaseQuantity(IdProd, decQuan);
        }
    }
    
    public void CreateReturn(Long IdPurchase, String IdRProd, int count) throws Exception {
        Purchase puch = purchasesImpl.getById(IdPurchase);
        if (puch == null)
        {
            throw new MyException("Покупки с таким ID не существует!");
        }
        if (puch.getStatus() == false)
        {
            throw new MyException("Покупка еще не оплачена! Возврат невозможен.");
        }
        SelectedProduct prodInPurch = puch.getList().get(IdRProd);
        if (prodInPurch == null) {
            throw new MyException("Товара с таким ID в покупке нет!");
        }
        if (prodInPurch.getQuantity() < count) {
            throw new MyException("Невозможно вернуть товара больше чем куплено!");
        }
        ReturnableProduct RetProd = new ReturnableProduct (prodInPurch.getProduct(), count);
        Map <String, ReturnableProduct> retPList = new HashMap();
        retPList.put(IdRProd, RetProd);
        Return newReturn = new Return(retPList);
        returnsImpl.save(newReturn);
    }
    
    public void AddItemToReturn(Long IdPurchase, Long IdReturn, String IdRProd, int count) throws Exception {
        Return ret = returnsImpl.getById(IdReturn);
        Purchase puch = purchasesImpl.getById(IdPurchase);
        if (ret == null)
        {
            throw new MyException("Возврата с таким ID не существует!");
        }
        if (ret.getStatus() == true)
        {
            throw new MyException("Операция возврата завершена! Добавить товар невозможно.");
        }
        
        SelectedProduct prodInPurch = puch.getList().get(IdRProd);
        if (prodInPurch == null) {
            throw new MyException("Товара с таким ID в покупке нет!");
        }
        if (prodInPurch.getQuantity() < count) {
            throw new MyException("Невозможно вернуть товара больше чем куплено!");
        }
        ReturnableProduct find = ret.getObjectsOfReturn().get(IdRProd);
        if (find != null){
            int addCount = find.getQuantity()+count; 
            if (addCount > prodInPurch.getQuantity()) {
               throw new MyException("Невозможно вернуть товара больше чем куплено!");
            }
        }
        ReturnableProduct ReturnNewItem = new ReturnableProduct(prodInPurch.getProduct(), count);
        ret.addProductToReturn(ReturnNewItem);
        returnsImpl.UpdateReturn(ret, IdReturn);
    }
     
    public void CloseReturn(Long IdReturn) throws Exception {
        Return ret = returnsImpl.getById(IdReturn);
        if (ret == null)
        {
            throw new MyException("Возврата с таким ID не существует!");
        }
        if (ret.getStatus() == true)
        {
            throw new MyException("Операция возврата завершена!");
        }
        ret.confirmReturn();
        returnsImpl.UpdateReturn(ret, IdReturn);
        Map <String, ReturnableProduct> listOfProds = returnsImpl.getById(IdReturn).getObjectsOfReturn();
        Iterator<Map.Entry<String, ReturnableProduct>> entries = listOfProds.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, ReturnableProduct> entry = entries.next();
            String IdProd = entry.getValue().getProduct().getId();
            int incQuan = entry.getValue().getQuantity();
            stockImpl.IncreaseQuantity(IdProd, incQuan);
        }
    }
      
}

    

