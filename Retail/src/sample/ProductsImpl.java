/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author admin
 */
public class ProductsImpl implements Products {
    public Map<String, Product> products;
    
    public ProductsImpl (){
        this.products=new HashMap<>();
    }

    @Override
    public void CreateProductList() throws Exception{
        Product prod = new Product("7 209380 644345", new BigDecimal("500.5"), 200);
        Product prod1 = new Product("4 607099 091450", new BigDecimal("95.5"), 50);
        Product prod2 = new Product("4 607065 580087", new BigDecimal("14.5"), 100);
        Product prod3 = new Product("4 602112 101802", new BigDecimal("10.5"), 200);
        Product prod4 = new Product("7 622210 742018", new BigDecimal("110.5"), 50);
        Product prod5 = new Product("6 410500 090014", new BigDecimal("119.00"), 30);
        Product prod6 = new Product("8 410545 090014", new BigDecimal("519.00"), 300);
        Product prod7 = new Product("5 416470 090014", new BigDecimal("56.00"), 400);
        Product prod8 = new Product("3 410500 445614", new BigDecimal("47.00"), 100);
        Product prod9 = new Product("2 670500 090014", new BigDecimal("1192.00"), 300);
        products.put(prod.getId(), prod);
        products.put(prod1.getId(), prod1);
        products.put(prod2.getId(), prod2);
        products.put(prod3.getId(), prod3);
        products.put(prod4.getId(), prod4);
        products.put(prod5.getId(), prod5);
        products.put(prod6.getId(), prod6);
        products.put(prod7.getId(), prod7);
        products.put(prod8.getId(), prod8);
        products.put(prod9.getId(), prod9);        
    }

    @Override
    public Product getById(String id) {
        return products.get(id);
    }

    @Override
    public ArrayList<Product> ShowAllProducts () {
        ArrayList ListOfProducts = new ArrayList<>(products.entrySet());
        return ListOfProducts;
    }
    
    @Override
    public void UpdateProduct(Product newProduct, String Id) throws Exception {
        if (getById(Id)!=null) {
            products.put(Id, newProduct);;
        }
        else
            throw new Exception();

    }

}
