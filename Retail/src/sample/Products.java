/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Products {
    public void CreateProductList () throws Exception;

    public ArrayList<Product> ShowAllProducts();

    public Product getById(String id);
    
    public void UpdateProduct(Product newProduct, String Id) throws Exception;
}
