/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Payments {
    public void save(Payment pay);

    public ArrayList<Payment> ShowAllPayments();

    public Payment getById(String id);
    
    void UpdatePayment (Payment newPay) throws Exception;
}
