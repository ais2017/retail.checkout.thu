/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author admin
 */
public class ReturnsImpl implements Returns {
    private AtomicLong currentOrderId = new AtomicLong(0);
    
    public Long incrementAndGetId() {
        return currentOrderId.incrementAndGet();
    }

    public static Map<Long, Return> returns = new HashMap<>();

    @Override
    public void save(Return ret) {
        Long Id = incrementAndGetId();
        returns.put(Id, ret);
    }

    @Override
    public Return getById(Long id) {
        return returns.get(id);
    }

    @Override
    public ArrayList<Return> ShowAllReturns () {
        ArrayList ListOfReturns = new ArrayList<>(returns.entrySet());
        return ListOfReturns;
    }
    
    @Override
    public void UpdateReturn(Return newReturn, Long Id) throws Exception {
        if (getById(Id)!=null) {
            returns.put(Id, newReturn);;
        }
        else
            throw new Exception();

    }

}
