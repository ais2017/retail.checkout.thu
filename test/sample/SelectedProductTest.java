/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import sample.Product;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class SelectedProductTest {
    public Product prod; 
    
    public SelectedProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws MyException{
        prod = new Product("5 201360 644841", new BigDecimal("100.5"), 10);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Construction of class SelectedProduct.
     */
    @Test
    public void testConstructionFailsIfProductIsNull() throws MyException {
        System.out.println("ConstructionFailsIfProductIsNull");
        try {
            new SelectedProduct(null, 10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
    @Test
    public void testConstructionFailsIfQuantityIsNegative() throws MyException {
        System.out.println("ConstructionFailsIfQuantityIsNegative");
        try {
            new SelectedProduct(prod, -10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testAfterConstructionProd() throws MyException{
       System.out.println("testAfterConstructionProd");
       Product prod2 = new Product("5 201360 644856", new BigDecimal("50.5"), 20);
       SelectedProduct sprod = new SelectedProduct(prod2, 10);
       assertEquals(prod2, sprod.getProduct());
    }

    @Test
    public void testAfterConstructionQuantity() throws MyException{
       System.out.println("testAfterConstructionQuantity");
       Product prod2 = new Product("5 201360 644856", new BigDecimal("50.5"), 20);
       int count = 10;
       SelectedProduct sprod = new SelectedProduct(prod2, count);
       assertEquals(count, sprod.getQuantity());
    }
    
    
    /**
     * Test of AddQuantity method, of class SelectedProduct.
     */
    @Test
    public void testAddQuantity() throws MyException {
        System.out.println("AddQuantity");
        int addPoints = 0;
        SelectedProduct instance = new SelectedProduct(prod, 2);
        try {
           instance.AddQuantity(addPoints);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }

    /**
     * Test of ReduceQuantity method, of class SelectedProduct.
     */
    @Test
    public void testReduceQuantity() throws MyException {
        System.out.println("ReduceQuantity");
        int cutPoints = 5;
        SelectedProduct instance = new SelectedProduct(prod, 3);
        try {
           instance.ReduceQuantity(cutPoints);
           Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
        
    }
    
}
