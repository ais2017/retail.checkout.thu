/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import java.util.Map;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author admin
 */
public class PurchaseTest {
    public Product prod1, prod2, prod3, prod4, prod5; 
    public SelectedProduct sprod1, sprod2, sprod3, sprod4, sprod5; 
    public Purchase purchase;
    Map <String, SelectedProduct> pList;
    public Card paycard;
    public Payment pays;
   
    public PurchaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws MyException {
        prod1 = new Product("4 607099 091450", new BigDecimal("95.5"), 50);
        prod2 = new Product("4 607065 580087", new BigDecimal("14.5"), 100);
        prod3 = new Product("4 602112 101802", new BigDecimal("10.5"), 200);
        prod4 = new Product("7 622210 742018", new BigDecimal("110.5"), 50);
        prod5 = new Product("6 410500 090014", new BigDecimal("119.00"), 30);
        sprod1 = new SelectedProduct(prod1, 2);
        sprod2 = new SelectedProduct(prod2, 5);
        sprod3 = new SelectedProduct(prod3, 3);
        sprod4 = new SelectedProduct(prod4, 1);
        sprod5 = new SelectedProduct(prod5, 1);
        pList = new HashMap();
        pList.put(sprod1.getProduct().getId(), sprod1);
        pList.put(sprod2.getProduct().getId(), sprod2);
        pList.put(sprod3.getProduct().getId(), sprod3);
        pList.put(sprod4.getProduct().getId(), sprod4);
        pList.put(sprod5.getProduct().getId(), sprod5);
        purchase = new Purchase(pList);
        paycard = new Card("1234 5678 9101 1112", 100);
        pays = new Payment("10","Cash",purchase);
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of Construction of class Purchase.
     */
    @Test
    public void testConstructionFailsIfProdListIsNull() throws MyException {
        System.out.println("ConstructionFailsIfProdListIsNull");
        try {
            new Purchase(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    /**
     * Test of AddProduct method, of class Purchase.
     */
    @Test
    public void testAddProduct1() throws MyException {
        System.out.println("testAddProduct1");
        SelectedProduct addProd;
        addProd = new SelectedProduct(prod2, 2);
        String key = addProd.getProduct().getId();
        int quant = purchase.getList().get(key).getQuantity() + addProd.getQuantity();
        purchase.addProduct(addProd);
        SelectedProduct fProd = purchase.getList().get(key);
        int result = fProd.getQuantity();
        assertEquals(quant, result);
    }
    
    @Test
    public void testAddProduct2() throws MyException {
        System.out.println("testAddProduct2");
        Product prd;
        SelectedProduct addProd;
        prd = new Product("4 607085 860060", new BigDecimal("70.00"), 30);
        addProd = new SelectedProduct(prd, 1);
        purchase.addProduct(addProd);
        String key = addProd.getProduct().getId();
        SelectedProduct result = purchase.getList().get(key);
        assertEquals(addProd, result);
    }
    
     /**
     * Test of DeleteProduct method, of class Purchase.
     */
    @Test
    public void testDeleteProduct1() throws MyException {
        System.out.println("testDeleteProduct1");
        SelectedProduct delProd;
        delProd = new SelectedProduct(new Product("4 607099 000000", new BigDecimal("95.5"), 50), 2);
        try {
            purchase.deleteProduct(delProd);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testDeleteProduct2() throws MyException {
        System.out.println("testDeleteProduct2");
        SelectedProduct delProd;
        delProd = new SelectedProduct(prod5, 2);
        try {
            purchase.deleteProduct(delProd);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testDeleteProduct3() throws MyException {
        System.out.println("testDeleteProduct3");
        SelectedProduct delProd;
        delProd = new SelectedProduct(prod5, 1);
        purchase.deleteProduct(delProd);
        String key = delProd.getProduct().getId();
        SelectedProduct result = purchase.getList().get(key);
        assertEquals(result, null);
    }
    
    @Test
    public void testDeleteProduct4() throws MyException {
        System.out.println("testDeleteProduct4");
        SelectedProduct delProd;
        delProd = new SelectedProduct(prod2, 2);
        String key = delProd.getProduct().getId();
        int quant = purchase.getList().get(key).getQuantity() - delProd.getQuantity();
        purchase.deleteProduct(delProd);
        SelectedProduct fProd = purchase.getList().get(key);
        int result = fProd.getQuantity();
        assertEquals(result, quant);
    }

    /**
     * Test of setCard method, of class Purchase.
     */
     
    @Test
    public void testSetCard() throws MyException {
        System.out.println("setCard");
        purchase.setCard(paycard);
        Card result = purchase.getCard();
        assertEquals(paycard, result);
    }

   
    /**
     * Test of setPayment method, of class Purchase.
     */
     
    @Test
    public void testSetPayment() throws MyException {
        System.out.println("SetPayment");
        purchase.setPayment(pays);
        Payment result = purchase.getPayment();
        assertEquals(pays, result);
    }
    
    /**
     * Test of closePurchase method, of class Purchase.
     */
    
    @Test
    public void testClosePurchase() throws Exception {
        System.out.println("testClosePurchase");
        purchase.closePurchase();
        assertEquals(purchase.getStatus(), true);
    }

    /**
     * Test of SummOfPurchase method, of class Purchase.
     */
    
    @Test
    public void testSummOfPurchase() throws MyException {
        System.out.println("testSummOfPurchase");
        BigDecimal result = purchase.SummOfPurchase(); 
        BigDecimal summ = new BigDecimal("524.50");
        assertEquals(summ, result);
      
    }
    
    @Test
    public void testPaymentWithPoints1() throws MyException {
        System.out.println("testPaymentWithPoints1");
        try {
            purchase.PaymentWithPoints(50);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    /*@Test
    public void testPaymentWithPoints2() throws MyException {
        System.out.println("testPaymentWithPoints2");
        purchase.setCard(paycard);
        try {
            purchase.PaymentWithPoints(500);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }*/
    
    @Test
    public void testPaymentWithPoints2() throws MyException {
        System.out.println("testPaymentWithPoints3");
        purchase.setCard(paycard);
        int qpnt = purchase.getCard().getQuantity(); 
        int reduct = 50;
        purchase.PaymentWithPoints(reduct);
        int result = purchase.getCutPoints();
        int res = qpnt - purchase.getCard().getQuantity();
        assertEquals(reduct, result);
        assertEquals(reduct, res);
    }
    
}
