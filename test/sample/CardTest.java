/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class CardTest {
    
    public CardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
    @Test
    public void testConstructionFailsIfIdIsNull() throws MyException {
        System.out.println("ConstructionFailsIfIdIsNullOrEmpty");
        try {
            new Card(null, 500);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfIdIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfIdIsEmpty");
        try {
            new Card("", 200);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfQuantityIsNegative() throws MyException {
        System.out.println("ConstructionFailsIfQuantityIsNegative");
        try {
            new Card("1234 5678 9101 1112", -20);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
    /**
     * Test of getId method, of class Card.
     */
    @Test
    public void testGetId() throws MyException{
       System.out.println("testGetId");
       String Id = "1234 5678 7777 1112";
       Card card = new Card(Id, 100);
       assertEquals(Id, card.getId());
    }

    /**
     * Test of getQuantity method, of class Card.
     */
    @Test
    public void testGetQuantity() throws MyException {
       System.out.println("testGetQuantity");
       int Quantity = 500;
       Card card = new Card("1234 5678 7777 1112", Quantity);
       assertEquals(Quantity, card.getQuantity());
    }

    /**
     * Test of AddQuantity method, of class Card.
     */
    @Test
    public void testAddQuantity() throws MyException {
        System.out.println("AddQuantity");
        int addPoints = 0;
        Card instance = new Card("1234 5678 7777 1112", 500);
        try {
            instance.AddQuantity(addPoints);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }

    /**
     * Test of ReduceQuantity method, of class Card.
     */
    @Test
    public void testReduceQuantity() throws MyException {
        System.out.println("ReduceQuantity");
        Card card = new Card("1234 5678 7777 1112", 500);
        int cutPoints = 100;
        int res = card.getQuantity() - cutPoints;
        card.ReduceQuantity(cutPoints);
        assertEquals(res, card.getQuantity());   
    }
    
}
