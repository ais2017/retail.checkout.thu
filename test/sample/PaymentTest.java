/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class PaymentTest {
    public Payment pay1, pay2;
    public Product prod1, prod2, prod3, prod4, prod5; 
    public SelectedProduct sprod1, sprod2, sprod3, sprod4, sprod5; 
    public Purchase purchase, purchase2;
    Map <String, SelectedProduct> pList, pList2;
    
    public PaymentTest() {
    }
    
    @Before
    public void setUp() throws MyException {
        prod1 = new Product("4 607099 091450", new BigDecimal("95.5"), 50);
        prod2 = new Product("4 607065 580087", new BigDecimal("14.5"), 100);
        prod3 = new Product("4 602112 101802", new BigDecimal("10.5"), 200);
        prod4 = new Product("7 622210 742018", new BigDecimal("110.5"), 50);
        prod5 = new Product("6 410500 090014", new BigDecimal("119.00"), 30);
        sprod1 = new SelectedProduct(prod1, 2);
        sprod2 = new SelectedProduct(prod2, 5);
        sprod3 = new SelectedProduct(prod3, 3);
        sprod4 = new SelectedProduct(prod4, 1);
        sprod5 = new SelectedProduct(prod5, 1);
        pList = new HashMap();
        pList2 = new HashMap();
        pList.put(sprod1.getProduct().getId(), sprod1);
        pList.put(sprod2.getProduct().getId(), sprod2);
        pList2.put(sprod3.getProduct().getId(), sprod3);
        pList2.put(sprod4.getProduct().getId(), sprod4);
        pList2.put(sprod5.getProduct().getId(), sprod5);
        purchase = new Purchase(pList);
        purchase2 = new Purchase(pList2);
        pay1 = new Payment("1", "Cash", purchase);
        pay2 = new Payment("2", "Card", purchase2);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testConstructionFailsIfIdIsNull() throws MyException {
        System.out.println("ConstructionFailsIfIdIsNull");
        try {
            new Payment(null, "Cash", purchase);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfMethodIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfMethodIsEmpty");
        try {
            new Payment("3", "", purchase);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfObjectIsNull() throws MyException {
        System.out.println("ConstructionFailsIfObjectIsNull");
        try {
            new Payment("5", "Cash", null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testAfterConstructionId() throws MyException{
       System.out.println("testAfterConstructionId");
       String Id = "9";
       Payment pay = new Payment(Id,"Card",purchase2);
       assertEquals(Id, pay.getIdPay());
    }

    @Test
    public void testAfterConstructionMethod() throws MyException{
       System.out.println("testAfterConstructionMethod");
       String Met = "Card";
       Payment pay = new Payment("6", Met, purchase2);
       assertEquals(Met, pay.getMethod());
    }
    
    @Test
    public void testAfterConstructionObject() throws MyException{
       System.out.println("testAfterConstructionObject");
       Payment pay = new Payment("7", "Card",purchase2);
       assertEquals(purchase2, pay.getObjectOfPayment());
    }
    
    /**
     * Test of setMethod method, of class Payment.
     */
    @Test
    public void testSetMethod() throws Exception {
        System.out.println("testSetMethod");
        try {
            pay1.setMethod(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }

    /**
     * Test of confirmPayment method, of class Payment.
     */
    @Test
    public void testConfirmPayment() throws Exception {
        System.out.println("testConfirmPayment");
        pay2.confirmPayment();
        assertEquals(pay2.getStatus(), true);
    }

    /**
     * Test of rejectPayment method, of class Payment.
     */
    @Test
    public void testRejectPayment() throws Exception {
        System.out.println("testRejectPayment");
        try {
            pay1.rejectPayment();
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
}
